#include <node.h>


namespace calculate {
  
  using v8::Context;
  using v8::Exception;
  using v8::FunctionCallbackInfo;
  using v8::Isolate;
  using v8::Local;
  using v8::Number;
  using v8::Object;
  using v8::String;
  using v8::Value;

  void Sum(const FunctionCallbackInfo<Value>&args) {
    
      Isolate* isolate = args.GetIsolate();

      auto  limit = args[0].As<Number>()->Value();
      double x=2676.2342378, y= 6232.27372;

      for(int i=0; i<limit; i++) {
        x+=y;
      }

      auto total = Number::New(isolate,x);
      args.GetReturnValue().Set(total);

  }

  void Substract(const FunctionCallbackInfo<Value>&args) {
    
      Isolate* isolate = args.GetIsolate();
      int x=10, y=5;
      x-=y;
      auto total = Number::New(isolate,x);
      args.GetReturnValue().Set(total);

  }
  

  void Initialize(Local<Object> exports) {
    NODE_SET_METHOD(exports,"sum", Sum);
    NODE_SET_METHOD(exports,"sub", Substract);
  }

  NODE_MODULE(NODE_GYP_MODULE_NAME, Initialize);

}



















/* cpp program    to demonstrate actual time take by c++ compilation


#include <iostream>
#include <ctime>

using namespace std;


int main() {

    int i, j;
    clock_t start, end;
    
    start = clock();
    
    double x=2676.2342378, y= 6232.27372;
    

      for( i=0; i<100000000; i++) {
        x+=y;
      }
      
    end = clock();
    cout<<"Time Cost: "<< (1000.0 * (end - start))/CLOCKS_PER_SEC<<endl;
    
    cout<<"a = "<< x;
    return 0;
}

output --

Time Cost: 352.16
a = 6.23227e+11

*/