const calculator = require('./build/Release/calculate')


const Js = (limit) => {

  let x = 2676.2342378, y = 6232.27372
  for (let i = 0; i < limit; i++) {
    x += y;
  }
  return x;
}

console.time('Js Time Taken')
console.log('Js result', Js(100000000))
console.timeEnd('Js Time Taken')
console.time('c++ time taken')
console.log('c++ result', calculator.sum(100000000))
console.timeEnd('c++ time taken')
